var debug = process.env.NODE_ENV !== "production";
const path = require('path');

var webpack = require('webpack');
module.exports = {
  context: __dirname, 
  devtool: debug ? "inline-sourcemap" : null,
  entry: "./js/scripts.js",
  output: {
    path: path.resolve(__dirname, "js"), 
    filename: "scripts.min.js",
    publicPath: "/js/",
  },
  optimization: {
    minimize: !debug
  },
  plugins: debug ? [] : [
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.optimize.UglifyJsPlugin({ mangle: false, sourcemap: false }),
  ],
  module: {
      rules: [{
          test: /\.js?$/,
          exclude: /(node_modules|bower_components)/,
          use: {
              loader: "babel-loader"
          }
      }],
  },
  devServer: {
    watchOptions: {
      poll: true
    }
   }
};
